
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);
    
    cppbuild::Target target("project");
    
    target.files({
        "program.cpp" 
    });

    target.libraries({
        "glfw3",
        "opengl32",
        "gdi32",
        "library",
    });

    target.includeDirs({
        "%WIN_CPP_PREFIX_PATH%\\include",
        "..\\library01"
    });

    target.libraryDirs({
        "%WIN_CPP_PREFIX_PATH%\\lib",
        "..\\library01\\build"
    });

    return 0;
}
