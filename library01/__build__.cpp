
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);
    
    cppbuild::Target target("library", cppbuild::TargetTypes::StaticLibrary);

    target.includeDirs({
        "%WIN_CPP_PREFIX_PATH%\\include",
    });

    target.files({
        "library.cpp" 
    });

    return 0;
}
